FROM adoptopenjdk/openjdk11:latest
VOLUME /tmp
EXPOSE 8080
COPY . /app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar", "-Dspring.profiles.active=prod", "/app.jar"]
